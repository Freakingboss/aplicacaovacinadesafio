package com.example.demo;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ActiveProfiles( value = "test" )
@RunWith( SpringRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@DirtiesContext( classMode = DirtiesContext.ClassMode.BEFORE_CLASS )
public class AplicacaoVacinaImplantacaoTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("se aplicacao vacina inserida!")
    public void seAplicacaoVacinaInserida() throws Exception {

        final String requisicaoAplicacaoVacina = "{\"email\": \"joaozinho@joao.com\"," +
                " \"nomeVacina\": \"coronaVac\"," +
                " \"dataDeVacinacao\": \"2000-12-14\"}";

        mvc.perform(MockMvcRequestBuilders
                .post("/cadastro/aplicacao-vacina")
                .contentType( MediaType.APPLICATION_JSON )
                .content( requisicaoAplicacaoVacina ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isCreated() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE) )
                .andExpect( MockMvcResultMatchers.jsonPath("$.email").value("joaozinho@joao.com") );

    }

    @Test
    @DisplayName("se email invalido!")
    public void seEmailInvalido() throws Exception {

        final String requisicaoAplicacaoVacina = "{\"email\": \"joaozinhojoao.com\"," +
                " \"nomeVacina\": \"coronaVac\"," +
                " \"dataDeVacinacao\": \"2000-12-14\"}";

        mvc.perform(MockMvcRequestBuilders
                .post("/cadastro/aplicacao-vacina")
                .contentType( MediaType.APPLICATION_JSON )
                .content( requisicaoAplicacaoVacina ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")) )
                .andExpect( MockMvcResultMatchers.content().string("Dados invalidos."));

    }

}
