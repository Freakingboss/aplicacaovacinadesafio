package com.example.demo;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ActiveProfiles( value = "test" )
@RunWith( SpringRunner.class )
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@DirtiesContext( classMode = DirtiesContext.ClassMode.BEFORE_CLASS )
public class UsuarioImplantacaoTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("se usuario inserido!")
    public void seUsuarioInserido() throws Exception {

        final String requisicaoUsuario = "{\"email\": \"joaozinho@joao.com\"," +
                " \"nomeUsuario\": \"Joao\"," +
                " \"cpf\": \"342.521.570-13\"," +
                " \"dataDeNascimento\": \"1990-11-18\"}";

        mvc.perform(MockMvcRequestBuilders
                .post("/cadastro/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content( requisicaoUsuario ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isCreated() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE) )
                .andExpect( MockMvcResultMatchers.jsonPath("$.email").value("joaozinho@joao.com"))
                .andExpect( MockMvcResultMatchers.jsonPath("$.nomeUsuario").value("Joao"))
                .andExpect( MockMvcResultMatchers.jsonPath("$.id").value(1L));

    }

    @Test
    @DisplayName("se cpf invalido!")
    public void seCpfInvalido() throws Exception{

        final String requisicaoUsuario = "{\"email\": \"joaozinho@joao.com\"," +
                " \"nomeUsuario\": \"Joao\"," +
                " \"cpf\": \"0-13\"," +
                " \"dataDeNascimento\": \"1990-11-18\"}";

        mvc.perform(MockMvcRequestBuilders
                .post("/cadastro/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content( requisicaoUsuario ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")) )
                .andExpect( MockMvcResultMatchers.content().string("Dados invalidos."));
    }

    @Test
    @DisplayName("se email invalido!")
    public void seEmailInvalido() throws Exception{

        final String requisicaoUsuario = "{\"email\": \"joaozinhojoao.com\"," +
                " \"nomeUsuario\": \"Joao\"," +
                " \"cpf\": \"342.521.570-13\"," +
                " \"dataDeNascimento\": \"1990-11-18\"}";

        mvc.perform(MockMvcRequestBuilders
                .post("/cadastro/user")
                .contentType( MediaType.APPLICATION_JSON )
                .content( requisicaoUsuario ))
                .andDo( MockMvcResultHandlers.print() )
                .andExpect( MockMvcResultMatchers.status().isBadRequest() )
                .andExpect( MockMvcResultMatchers.content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect( MockMvcResultMatchers.content().string("Dados invalidos."));
    }

}
