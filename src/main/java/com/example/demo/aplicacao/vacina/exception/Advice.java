package com.example.demo.aplicacao.vacina.exception;

import com.example.demo.usuario.exception.UsuarioException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

public class Advice {

    @ExceptionHandler( {MethodArgumentNotValidException.class} )
    public ResponseEntity methodsArgumentedInvalid(MethodArgumentNotValidException e ){
        return new ResponseEntity("Dados invalidos.", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler( {ConstraintViolationException.class} )
    public ResponseEntity dadosInvalidos( ConstraintViolationException e ){
        return new ResponseEntity("Dados invalidos.", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler( {UsuarioException.class} )
    public ResponseEntity excecaoUsuario( UsuarioException e ){
        return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
