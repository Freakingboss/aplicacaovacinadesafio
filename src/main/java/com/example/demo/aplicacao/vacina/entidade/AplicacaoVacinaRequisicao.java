package com.example.demo.aplicacao.vacina.entidade;

import com.example.demo.aplicacao.vacina.service.AplicacaoVacinaService;

import java.time.LocalDate;

public class AplicacaoVacinaRequisicao {

    private final AplicacaoVacina aplicacaoVacina;

    public AplicacaoVacinaRequisicao(){
        aplicacaoVacina = new AplicacaoVacina();
    }

    public void setEmail(String email) {
        aplicacaoVacina.contato(email);
    }

    public void setNomeVacina(String nomeVacina) {
        aplicacaoVacina.comNome(nomeVacina);
    }

    public void setDataDeVacinacao(LocalDate dataDeVacinacao) {
        aplicacaoVacina.vacinouEm(dataDeVacinacao);
    }

    public AplicacaoVacina cadastrar(AplicacaoVacinaService service) {
        return service.cadastrarAplicacaoVacina(aplicacaoVacina);
    }
}
