package com.example.demo.aplicacao.vacina.service;

import com.example.demo.aplicacao.vacina.entidade.AplicacaoVacina;

public interface AplicacaoVacinaService {

    AplicacaoVacina cadastrarAplicacaoVacina(AplicacaoVacina aplicacaoVacina);
}
