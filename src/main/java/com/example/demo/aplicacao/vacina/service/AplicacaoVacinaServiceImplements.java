package com.example.demo.aplicacao.vacina.service;

import com.example.demo.aplicacao.vacina.entidade.AplicacaoVacina;
import com.example.demo.aplicacao.vacina.RepositorioAplicacaoVacina;
import org.springframework.stereotype.Service;

@Service
public final class AplicacaoVacinaServiceImplements implements AplicacaoVacinaService {

    private final RepositorioAplicacaoVacina repositorioAplicacaoVacina;

    public AplicacaoVacinaServiceImplements(final RepositorioAplicacaoVacina repositorioAplicacaoVacina){
        this.repositorioAplicacaoVacina = repositorioAplicacaoVacina;
    }

    @Override
    public AplicacaoVacina cadastrarAplicacaoVacina(final AplicacaoVacina aplicacaoVacina){
        return aplicacaoVacina.salvarAplicacaoVacina(repositorioAplicacaoVacina);
    }
}
