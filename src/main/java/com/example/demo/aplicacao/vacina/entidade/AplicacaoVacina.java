package com.example.demo.aplicacao.vacina.entidade;

import com.example.demo.aplicacao.vacina.RepositorioAplicacaoVacina;
import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Entity
public class AplicacaoVacina {

    public AplicacaoVacina(){}

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotNull
    @Email
    private String email;

    @NotBlank
    private String nomeVacina;

    @NotNull
    private LocalDate dataDeVacinacao;

    public void contato(String email) {
        this.email = email;
    }

    public void comNome(String nomeVacina) {
        this.nomeVacina = nomeVacina;
    }

    public void vacinouEm(LocalDate dataDeVacinacao) {
        this.dataDeVacinacao = dataDeVacinacao;
    }

    public AplicacaoVacina salvarAplicacaoVacina(RepositorioAplicacaoVacina repositorioAplicacaoVacina){
        return repositorioAplicacaoVacina.save(this);
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
