package com.example.demo.aplicacao.vacina.controller;

import com.example.demo.aplicacao.vacina.entidade.AplicacaoVacina;
import com.example.demo.aplicacao.vacina.entidade.AplicacaoVacinaRequisicao;
import com.example.demo.aplicacao.vacina.service.AplicacaoVacinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class AplicacaoVacinaController {

    private final AplicacaoVacinaService service;

    @Autowired
    private AplicacaoVacinaController(AplicacaoVacinaService service){
        this.service = service;
    }

    @PostMapping(path="/cadastro/aplicacao-vacina")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody AplicacaoVacina cadastrarAplicacaoVacina(@RequestBody @Validated AplicacaoVacinaRequisicao aplicacaoVacinaRequisicao){

        return aplicacaoVacinaRequisicao.cadastrar(service);

    }
}
