package com.example.demo.aplicacao.vacina;

import com.example.demo.aplicacao.vacina.entidade.AplicacaoVacina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioAplicacaoVacina extends JpaRepository<AplicacaoVacina, Long> {
}
