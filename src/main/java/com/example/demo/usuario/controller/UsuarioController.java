package com.example.demo.usuario.controller;

import com.example.demo.usuario.entidade.UsuarioRequisicao;
import com.example.demo.usuario.service.UsuarioService;
import com.example.demo.usuario.entidade.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsuarioController {

    private final UsuarioService service;

    @Autowired
    private UsuarioController(UsuarioService service){
        this.service = service;
    }

    @PostMapping(path="/cadastro/user")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@RequestBody @Validated UsuarioRequisicao usuarioRequisicao){

        return usuarioRequisicao.cadastrar(service);

    }
}
