package com.example.demo.usuario.exception;

public class UsuarioException extends RuntimeException {
    public UsuarioException(String s) {
        super(s);
    }
}
