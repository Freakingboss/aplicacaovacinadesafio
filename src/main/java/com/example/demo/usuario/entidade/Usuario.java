package com.example.demo.usuario.entidade;

import com.example.demo.usuario.RepositorioUsuario;
import com.example.demo.usuario.exception.UsuarioException;
import com.sun.istack.NotNull;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

@Entity
public class Usuario {

    public Usuario() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(nomeUsuario, usuario.nomeUsuario) && Objects.equals(cpf, usuario.cpf) && Objects.equals(email, usuario.email) && Objects.equals(dataDeNascimento, usuario.dataDeNascimento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nomeUsuario, cpf, email, dataDeNascimento);
    }

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotBlank
    private String nomeUsuario;

    @NotBlank
    @Size(max = 14)
    @CPF
    private String cpf;

    @NotNull
    @Email
    private String email;

    @NotNull
    private LocalDate dataDeNascimento;

    public void comNome(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public void paraCPF(String cpf) {
        this.cpf = cpf;
    }

    public void contato(String email) {
        this.email = email;
    }

    public void nasceuEm(LocalDate dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    private Optional<Usuario> verificarSeExistemUsuariosDuplicados(RepositorioUsuario repositorioUsuario){
        return repositorioUsuario.findFirstUsuarioByEmailOrCpf(email,cpf);
    }

    public Usuario salvarUsuario(RepositorioUsuario repositorioUsuario){
        verificarSeExistemUsuariosDuplicados(repositorioUsuario)
                .ifPresent(e->{throw new UsuarioException("Nao eh possivel salvar usuarios duplicados!");});
        return repositorioUsuario.save(this);
    }

    public Long getId() {
        return id;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public String getEmail() {
        return email;
    }

}
