package com.example.demo.usuario.entidade;

import com.example.demo.usuario.service.UsuarioService;

import java.time.LocalDate;

public class UsuarioRequisicao {

    private final Usuario usuario;

    public UsuarioRequisicao(){
        usuario = new Usuario();
    }

    public void setNomeUsuario(String nomeUsuario) {
        usuario.comNome(nomeUsuario);
    }

    public void setCpf(String cpf) {
        usuario.paraCPF(cpf);
    }

    public void setEmail(String email) {
        usuario.contato(email);
    }

    public void setDataDeNascimento(LocalDate dataDeNascimento) {
        usuario.nasceuEm(dataDeNascimento);
    }

    public Usuario cadastrar(UsuarioService service) {
        return service.cadastrarUsuario(usuario);
    }
}
