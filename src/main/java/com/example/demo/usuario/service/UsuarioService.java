package com.example.demo.usuario.service;

import com.example.demo.usuario.entidade.Usuario;

public interface UsuarioService {

    Usuario cadastrarUsuario(Usuario usuario);
}
