package com.example.demo.usuario.service;

import com.example.demo.usuario.RepositorioUsuario;
import com.example.demo.usuario.entidade.Usuario;
import org.springframework.stereotype.Service;

@Service
public final class UsuarioServiceImplements implements UsuarioService {

    private final RepositorioUsuario repositorioUsuario;

    public UsuarioServiceImplements(final RepositorioUsuario repositorioUsuario){
        this.repositorioUsuario = repositorioUsuario;
    }

    @Override
    public Usuario cadastrarUsuario(final Usuario usuario){
        return usuario.salvarUsuario(repositorioUsuario);
    }
}